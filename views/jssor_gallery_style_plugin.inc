<?php

/**
 * @file
 * Contains the jssor_gallery style plugin.
 */

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 */
class jssor_gallery_style_plugin extends views_plugin_style {
  function option_definition() {
    $options=parent::option_definition();
    $options['style']=array('default' => 'default');
    $options['uses_fields']=array('default' => FALSE);
    $options['image_field']=array('default' => FALSE);
    $options['thumbnail_field']=array('default' => FALSE);
    $options['caption_fields']=array('default' => array());
    $options['content_fields']=array('default' => array());
    $options['ajax']=array('default' => 0);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Build the list of skins as options.
    $styles=_jssor_gallery_get_skins();

    $form['description'] = array(
      '#type' => 'markup',
      '#value' => '<div class="messages">' . t('The jssor_gallery style is affected by several other settings within the display. Enable the "Use AJAX" option on your display to have items loaded dynamically. The "Items to display" option will determine how many items are preloaded into the carousel on each AJAX request.').'</div>',
    );

    $form['style'] = array(
      '#type' => 'select',
      '#title' => t('Skin'),
      '#default_value' => $this->options['style'],
      '#options' => $styles,
      '#description' => t('Skins may be provided by other modules. Set to "None" if your theme includes carousel theming directly in style.css or another stylesheet. "None" does not include any built-in navigation, arrows, or positioning at all.'),
    );
    
    /*
    $form['ajax'] = array(
      '#type' => 'checkbox',
      '#title' => t('AJAX load pages'),
      '#default_value' => $this->options['ajax'],
      '#description' => t('The number of items set in the pager settings will be preloaded. All additional pages will be loaded by AJAX as needed.'),
    );
    */
    
    $handlers=$this->display->handler->get_handlers('field');
    if (!empty($handlers)) {
      $form['uses_fields']=array('#type' => 'value', '#value' => TRUE);
      $field_names=$this->display->handler->get_field_labels();
      $image_fields=array();
      foreach ($field_names as $name => $label) {
        $field_info=field_info_field($name);
        if ($field_info['type'] == 'image') $image_fields[$name]=$label;
      }

      $form['image_field']=array(
        '#type' => 'select',
        '#title' => t('Main image field'),
        '#multiple' => FALSE,
        '#options' => array(0 => t('None'))+$image_fields,
        '#default_value' => $this->options['image_field'],
      );
      $form['thumbnail_field']=array(
        '#type' => 'select',
        '#title' => t('Thumbnail field'),
        '#multiple' => FALSE,
        '#options' => array(0 => t('None'))+$image_fields,
        '#default_value' => $this->options['thumbnail_field'],
      );
      $form['caption_fields']=array(
        '#type' => 'select',
        '#title' => t('Caption fields'),
        '#multiple' => TRUE,
        '#options' => $field_names,
        '#default_value' => $this->options['caption_fields'],
      );
      $form['content_fields']=array(
        '#type' => 'select',
        '#title' => t('Content fields'),
        '#multiple' => TRUE,
        '#options' => $field_names,
        '#default_value' => $this->options['content_fields'],
        '#description' => t('Fields not used expicitly as any of the above, but as normal slide content.')
      );
    }
    else $form['uses_fields']=array('#type' => 'value', '#value' => FALSE);
  }

  function validate() {
    $errors = parent::validate();
    if ($this->display->handler->use_pager()) {
      $errors[] = t('The jssor_gallery style cannot be used with a pager. Disable the "Use pager" option for this display.');
    }
    return $errors;
  }
  
  function render() {
    
    $rows=array();
    if (!$this->options['uses_fields'] && $this->uses_row_plugin()) {
      foreach ($this->view->result as $index => $row) {
        $this->view->row_index = $index;
        $rows[$index]=array(
          'type' => 'content',
          'content' => $this->row_plugin->render($row)
        );
      }
    }
    
    else {
      $entities=array();
      if (!empty($this->options['link_field']) || !empty($this->options['caption_fields']) || !empty($this->options['content_fields'])) {
        $renders=$this->view->style_plugin->render_fields($this->view->result);
      }

      foreach ($this->view->result as $index => $row) {
        if (!empty($this->options['image_field'])) {
          if (isset($row->{'field_'.$this->options['image_field']})) {
            $rendered=$row->{'field_'.$this->options['image_field']}[0]['rendered'];
            if (!empty($rendered['#image_style'])) {
              $rows[$index]['image_url']=image_style_url($rendered['#image_style'], $rendered['#item']['uri']);
            }
            else $rows[$index]['image_url']=file_create_url($rendered['#item']['uri']);
            if (!empty($rendered['#item']['title'])) $rows[$index]['title']=$rendered['#item']['title'];
            if (!empty($rendered['#item']['alt'])) $rows[$index]['alt']=$rendered['#item']['alt'];
          }
        }
        if (!empty($this->options['thumb_field'])) {
          if (isset($row->{'field_'.$this->options['thumb_field']})) {
            $rendered=$row->{'field_'.$this->options['thumb_field']}[0]['rendered'];
            if (!empty($rendered['#image_style'])) {
              $rows[$index]['image_url']=image_style_url($rendered['#image_style'], $rendered['#item']['uri']);
            }
            else $rows[$index]['thumb_url']=file_create_url($rendered['#item']['uri']);
          }
        }
        
        if (!empty($this->options['link_field'])) {
          //Todo
        }
        
        if (!empty($this->options['caption_fields'])) {
          $caption='';
          foreach ($this->options['caption_fields'] as $caption_field) {
            if ($caption_field) {
              $caption.=$renders[$index][$caption_field];
            }
          }
          if (!empty($caption)) {
            $rows[$index]['caption']=$caption;
          }
        }
        
        if (!empty($this->options['content_fields'])) {
          $content='';
          foreach ($this->options['caption_fields'] as $caption_field) {
            if ($caption_field) {
              $content.=$renders[$index][$caption_field];
            }
          }
          if (!empty($content)) {
            $rows[$index]['content']=$content;
          }
        }
      }
    }
        
    $output=theme('jssor_gallery', array(
      'settings' => $this->options,
      'style' => $this->options['style'],
      'slides' => $rows,
      'gallery_id' => 'jssor_'.$this->view->name.'_'.$this->view->current_display
    ));
    
    return $output;
  }

}
