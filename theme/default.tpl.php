<script>
  jssor_slider1_starter = function (containerId) {
    var options = {
      $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
      $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
      $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
      $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
      $UISearchMode: 0,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).

      $ThumbnailNavigatorOptions: {
        $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

        $Loop: 2,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
        $SpacingX: 3,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
        $SpacingY: 3,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
        $DisplayPieces: 6,                              //[Optional] Number of pieces to display, default value is 1
        $ParkingPosition: 204,                          //[Optional] The offset position to park thumbnail,

        $ArrowNavigatorOptions: {
          $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
          $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 6                                       //[Optional] Steps to go for each navigation request, default value is 1
        }
      }
    };

    var jssor_slider1 = new $JssorSlider$(containerId, options);

    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizes
    function ScaleSlider() {
      var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
      if (parentWidth)
        jssor_slider1.$ScaleWidth(Math.min(parentWidth, 720));
      else
        $Jssor$.$Delay(ScaleSlider, 30);
    }

    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);


    if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
      $Jssor$.$AddEvent(window, "resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
    }

    //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
    //    $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    //}
    //responsive code end
  };
</script>
<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->
<div id="slider1_container" class="slider-container">

  <!-- Loading Screen -->
  <div u="loading" class="loading">
    <div class="overlay"></div>
    <div class="img-wrapper"></div>
  </div>

  <!-- Slides Container -->
  <div u="slides" class="slides">
    <?php print $slides; ?>
  </div>
      
  <!-- Thumbnail Navigator Skin Begin -->
  <div u="thumbnavigator" class="navigator-default">
    <div class="bkg"></div>
    <!-- Thumbnail Item Skin Begin -->
    <div u="slides" class="nav-slides">
      <div u="prototype" class="p">
        <thumbnailtemplate class="i"></thumbnailtemplate>
        <div class="o" style="width: 99px; height: 66px;"></div>
      </div>
    </div>
    <!-- Thumbnail Item Skin End -->
    
    <!-- Arrow Navigator Skin Begin -->
    <!-- Arrow Left -->
    <span u="arrowleft" class="arrow-default-l"></span>
    <!-- Arrow Right -->
    <span u="arrowright" class="arrow-default-r"></span>
    <!-- Arrow Navigator Skin End -->
    
  </div>
  <!-- ThumbnailNavigator Skin End -->
  
  <!-- Trigger -->
  <script>
    jssor_slider1_starter('slider1_container');
  </script>

</div>
<!-- Jssor Slider End -->
    