<script>
  jssor_slider1_starter = function (containerId) {
    var options = {
      $AutoPlay: false, //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
      $ShowLoading: true,
      $SlideDuration: 500, //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
      $ArrowKeyNavigation: true,
      $PlayOrientation: 1, //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, default value is 1
      $DragOrientation: 1, //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 both, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
        
      $ThumbnailNavigatorOptions: {
        $Class: $JssorThumbnailNavigator$, //[Required] Class to create thumbnail navigator instance
        $ChanceToShow: 2, //[Required] 0 Never, 1 Mouse Over, 2 Always
        $ActionMode: 1,  //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
        $AutoCenter: 3, //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
        $Lanes: 1, //[Optional] Specify lanes to arrange thumbnails, default value is 1
        $SpacingX: 5, //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
        $SpacingY: 0, //[Optional] Vertical space between each thumbnail in pixel, default value is 0
        $DisplayPieces: 5, //[Optional] Number of pieces to display, default value is 1
        $ParkingPosition: 0, //[Optional] The offset position to park thumbnail
        $Orientation: 1, //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
        $DisableDrag: false, //[Optional] Disable drag or not, default value is false
        
        $ArrowNavigatorOptions: {
          $Class: $JssorArrowNavigator$, //[Requried] Class to create arrow navigator instance
          $ChanceToShow: 2, //[Required] 0 Never, 1 Mouse Over, 2 Always
          $AutoCenter: 2, //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
          $Steps: 1 //[Optional] Steps to go for each navigation request, default value is 1
        }
      },
      
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$, //[Requried] Class to create arrow navigator instance
        $ChanceToShow: 2, //[Required] 0 Never, 1 Mouse Over, 2 Always
        $AutoCenter: 0, //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
        $Steps: 1 //[Optional] Steps to go for each navigation request, default value is 1
      }
    };
    var jssor_slider1 = new $JssorSlider$(containerId, options);
    
    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizes
    function ScaleSlider() {
      var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
      if (parentWidth)
        jssor_slider1.$ScaleWidth(Math.max(Math.min(parentWidth, 1000), 300));
      else
        $Jssor$.$Delay(ScaleSlider, 30);
    }

    ScaleSlider();
    $Jssor$.$AddEvent(window, "load", ScaleSlider);

    if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
      $Jssor$.$AddEvent(window, "resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
    }

    if (navigator.userAgent.match(/(iPhone|iPod|iPad|IEMobile)/)) {
      $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
    }
    //responsive code end
  };
</script>
<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->
    <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 1018px; height: 572px;">

    <!-- Slides Container -->
    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1018px; height: 572px; overflow: hidden;">
      <?php print $slides; ?>
    </div>
  
    <!-- ThumbnailNavigator Skin Begin -->
    <div u="thumbnavigator" class="jssort12" style="cursor: default; position: absolute; width: 1018px; height: 100px; left:0px; bottom: 0px;">
    
      <div class="bkg"></div>
        <!-- Thumbnail Item Skin Begin -->
        
        <div u="slides" style="cursor: move;">
          <div u="prototype" class=p style="POSITION: absolute; WIDTH: 120px; HEIGHT: 90px; TOP: 0; LEFT: 0;">
            <thumbnailtemplate style="WIDTH: 120px; HEIGHT: 90px; border: none; position: absolute; TOP: 0; LEFT: 0; "></thumbnailtemplate>
            <div class="o" style="width: 120px; height: 90px;"></div>
          </div>
        </div>
      <!-- Thumbnail Item Skin End -->
      
            <!-- Arrow Navigator Skin Begin -->
            <!-- Arrow Left -->
            <span u="arrowleft" class="jssora11l" style="width: 37px; height: 37px; top: 0px; left: 5px;">
            </span>
            <!-- Arrow Right -->
            <span u="arrowright" class="jssora11r" style="width: 37px; height: 37px; top: 0px; right: 5px">
            </span>
            <!-- Arrow Navigator Skin End -->

    </div>
    <!-- ThumbnailNavigator Skin End -->
    <!-- Trigger -->
    <script>
        jssor_slider1_starter('slider1_container');
    </script>
</div>
<!-- Jssor Slider End -->